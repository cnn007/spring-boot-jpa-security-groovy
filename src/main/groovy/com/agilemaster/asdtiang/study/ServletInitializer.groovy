package com.agilemaster.asdtiang.study

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.web.SpringBootServletInitializer

import com.agilemaster.asdtiang.study.bootstrap.BootstrapService


class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		application.sources(GroovyAndJavaApplication)
	}
}
