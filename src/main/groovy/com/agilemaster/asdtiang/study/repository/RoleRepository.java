package com.agilemaster.asdtiang.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agilemaster.asdtiang.study.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	Role findByAuthority(String role);
}
