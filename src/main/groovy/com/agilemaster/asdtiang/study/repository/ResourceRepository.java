package com.agilemaster.asdtiang.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agilemaster.asdtiang.study.domain.Resource;

public interface ResourceRepository extends JpaRepository<Resource, Long> {
}


