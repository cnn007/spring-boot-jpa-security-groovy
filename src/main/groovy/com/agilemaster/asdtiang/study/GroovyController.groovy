package com.agilemaster.asdtiang.study

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/groovy")
class GroovyController {
	@RequestMapping(value = "/{user}", method = RequestMethod.GET)
	def   getUser(@PathVariable Long user) {
		Map map = new HashMap<String,String>();
		map.put("我是gaaaaaaaaa","Controller");
		return map;
}
	@RequestMapping(value = "/a/{user}", method = RequestMethod.GET)
	def   testLoad(@PathVariable Long user) {
		Map map = new HashMap<String,String>();
		map.put("testLoad","Controller");
		return map;
}
}
