package com.agilemaster.asdtiang.study


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.orm.jpa.EntityScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

import com.agilemaster.asdtiang.study.bootstrap.BootstrapService;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.agilemaster.asdtiang.study.repository")
@EntityScan(basePackages = "com.agilemaster.asdtiang.study.domain")
class GroovyAndJavaApplication {

    static void main(String[] args) {
        SpringApplication.run GroovyAndJavaApplication, args
		TestGroovy.sayGroovy();
		TestJava.sayJava();	
    }
}
